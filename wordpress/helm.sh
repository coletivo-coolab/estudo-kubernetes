#!/bin/bash

helm repo add bitnami https://charts.bitnami.com/bitnami
helm update
helm install my-wordpress --values=values.yaml bitnami/wordpress