#!/bin/bash

# Install kubctl: https://kubernetes.io/docs/tasks/tools/install-kubectl/
# Install minikube (for local Kubernetes): https://kubernetes.io/docs/setup/minikube/
# Install helm: https://helm.sh/
# Install kubectx (change between local/clouds): https://github.com/ahmetb/kubectx

# nginx-ingress
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx ingress-nginx/ingress-nginx
sleep 40
POD_NAME=$(kubectl get pods -l app.kubernetes.io/name=ingress-nginx -o jsonpath='{.items[0].metadata.name}')
kubectl exec -it $POD_NAME -- /nginx-ingress-controller --version

# cert-manager
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version v1.1.0 \
  --set installCRDs=true
kubectl get pods --namespace cert-manager

# Changing contexts
kubectx
# kubectx CONTEXT_NAME