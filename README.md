# Estudo Kubernetes

Rodar `./install.sh` para instalar ferramentas básicas:
- kubectl
- helm
- minikube
- kubectx

Modifique e rode o nginx-ingress: `kubectl apply -f nginx-ingress.yaml`

Depois modifique e rode o certificador de SSL `kubectl apply -f certificate_issuer.yaml`

Não esqueça de direcionar os DNSs ao IP externo do controlador do nginx.
Para conseguir o IP externo do controlador user algo assim:
`kubectl get all | grep nginx-controller | grep 80`


Depois entre em cada projeto e rode o `sh` que está dentro.

## TODO

- Adicionar [DEIS PaaS](https://web.teamhephy.com/)
- Adicionar mais serviços
