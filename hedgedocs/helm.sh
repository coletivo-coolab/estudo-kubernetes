#!/bin/bash

helm repo add nicholaswilde https://nicholaswilde.github.io/helm-charts/
helm repo update
helm install my-hedgedoc nicholaswilde/hedgedoc
