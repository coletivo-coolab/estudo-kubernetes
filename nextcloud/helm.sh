#!/bin/bash

# helm install my-nextcloud --values=values.yaml nextcloud/nextcloud
helm repo add nextcloud https://nextcloud.github.io/helm/
helm update
helm install my-nextcloud --values=values.yaml groundhog2k/nextcloud
